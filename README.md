# OpenML dataset: SportsCars

https://www.openml.org/d/43855

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data comprises the brand, type and model. For each car we have information about the cubic capacity (in cm3), the maximal power of engine (in kW), the maximal torque (in Nm), the number of seats, the weight of the car (in kg), the maximum engine speed (in rpm), the acceleration from 0 to 100km/h (in seconds), and the top speed (in km/h). In addition if the car is classified as a sports car (binary) and the tau value (see article).

This data is based on the data from the article &ldquo;What is a sports car&rdquo; by Ingenbleek&ndash;Lemaire (1988, ASTIN Bulletin 18/2, 175-187.). Unfortunately, only part of the original data set is still available. Therefore, it has been extended with additional cars which have been compiled from the internet.

The dataset was kindly created and provided by Simon Rentzmann and Mario V. Wuthrich.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43855) of an [OpenML dataset](https://www.openml.org/d/43855). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43855/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43855/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43855/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

